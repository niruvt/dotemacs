;; देखाव्यांकरिता योग्य पत्ता स्थापित करण्याकरिता
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
;; ग्रव्हबॉक्स हा देखावा निवडण्याकरिता
(load-theme (quote gruvbox-dark-soft) t)
;; धारिकांची संपूर्ण यादी पटलिकेत दाखवण्याकरिता
(ido-mode 1)
;; शब्दांना आपोआप पूर्ण करण्याकरिता
(company-mode 1)
(add-hook 'after-init-hook 'global-company-mode)
;; कंसांना ठळकपणे दाखवण्याकरिता
(show-paren-mode 1)
;; चालू ओळ ठळकपणे दाखवण्याकरिता.
(global-hl-line-mode 1)
;; *scratch* पटलावरील मजकूर बदलण्याकरिता.
(setq initial-scratch-message
      ";; ह्या पटलावरील काम जतन केले जाणार नाही.\n;; लिस्प आज्ञावलीच्या चाचण्यांकरिता हे पटल वापरता येऊ शकेल.")
;; टूल-बार लपवण्याकरिता.
(tool-bar-mode 0)
;; मेनू-बार लपवण्याकरिता.
(menu-bar-mode 0)
;; आरंभीचे पटल बंद करण्याकरिता.
(setq inhibit-startup-screen t)
;; शेलचे पत्ते वापरण्याकरिता.
;; (setenv "PATH" (concat "/usr/local/texlive/2020/bin/x86_64-linux/"
;;                       (getenv "PATH")))
;; (add-to-list 'exec-path "/usr/local/texlive/2020/bin/x86_64-linux/")
;; ऑटो-फिल-मोड चालू-बंद करण्याकरिता.
;; (मूलभूत अक्षरमर्यादा ८०).
(auto-fill-mode 1)
(global-set-key "\C-ct" 'auto-fill-mode)
(add-hook 'text-mode-hook #'auto-fill-mode)
(setq-default fill-column 80)
;; डिर-एड्-मोडमध्ये गुप्त धारिका दाखवण्या व लपवण्याकरिता.
;; (गुप्त धारिकांची मूलभूत अवस्था - लपवलेली. "h" ही कळ दाबून त्या पाहता (व पुन्हा लपवता) येतील.)
(require 'dired-x)
(add-hook 'dired-mode-hook
  (lambda ()
     (setq dired-omit-files
           "^\\.?#\\|^\\.$\\|^\\.\\.$\\|^\\.[a-zA-Z]")
     (dired-omit-mode 1)
     (local-set-key "h" 'dired-omit-mode)
))
;; मेल्पा आज्ञावलीचा वापर करण्याकरिता.
(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   '("melpa" . "https://melpa.org/packages/")
   t))
(package-initialize)
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
(getenv "PATH")
(setenv "PATH"
	(concat
	 "/usr/local/texlive/2020/bin/x86_64-linux" ":"
	 (getenv "PATH")))
(setq TeX-engine (quote luatex))
(setq LaTeX-command "lualatex -synctex=1")
(setq TeX-command-default "lualatex")
